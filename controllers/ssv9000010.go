//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000010/dao"
	"git.forms.io/isaving/sv/ssv9000010/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000010Controller struct {
	controllers.CommTCCController
}

func (*Ssv9000010Controller) ControllerName() string {
	return "Ssv9000010Controller"
}

// @Desc ssv9000010 controller
// @Description Entry
// @Param ssv9000010 body models.SSV9000010I true "body for user content"
// @Success 200 {object} models.SSV9000010O
// @router /ssv9000010 [post]
// @Author
// @Date 2020-12-12
func (c *Ssv9000010Controller) Ssv9000010() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000010Controller.Ssv9000010 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000010I := &models.SSV9000010I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000010I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000010I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000010 := &services.Ssv9000010Impl{}
	ssv9000010.New(c.CommTCCController)
	ssv9000010.Ssv900010I = ssv9000010I
	ssv9000010.O = dao.EngineCache
	ssv9000010Compensable := services.Ssv9000010Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000010, ssv9000010Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000010I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000010O, ok := rsp.(*models.SSV9000010O); ok {
		if responseBody, err := models.PackResponse(ssv9000010O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv9000010 Controller
// @Description ssv9000010 controller
// @Param Ssv9000010 body models.SSV9000010I true body for SSV9000010 content
// @Success 200 {object} models.SSV9000010O
// @router /create [post]
/*func (c *Ssv9000010Controller) SWSsv9000010() {
	//Here is to generate API documentation, no need to implement methods
}*/

package dao

import (
	"git.forms.io/legobank/legoapp/errors"

	//"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_evsbmxp struct {
	TransationDate          string  `xorm:"'transation_date'"`           //交易日期
	TransationTime          string  `xorm:"'transation_time'"`           //交易时间
	GlobalBizSeqNo          string  `xorm:"'global_biz_seqno' pk"`       //全局流水号
	SrcBizseqNo             string  `xorm:"'src_biz_seq_no'"`            //业务流水号
	SrcTimeStamp            string  `xorm:"'src_time_stamp'"`            //源系统工作日期时间
	SrcSysId                string  `xorm:"'src_sysid'"`                 //源系统编码
	SrcDcn                  string  `xorm:"'src_dcn'"`                   //源系统DCN
	OrgChannelType          string  `xorm:"'org_channel_type'"`          //交易发起渠道
	TxDeviceId              string  `xorm:"'tx_device_id'"`              //终端号
	TxDeptCode              string  `xorm:"'tx_dept_code'"`              //交易行所
	BnAcc                   string  `xorm:"'bn_acc'"`                    //账务行所
	TransationEm            string  `xorm:"'transation_em'"`             //交易柜员
	TransationType          string  `xorm:"'transation_type'"`           //交易方式
<<<<<<< HEAD
	PaySourceFunds          string  `xorm:"'pay_source_funds'"`          //转出方账号类型
	PayCusid                string  `xorm:"'pay_cusid'"`                 //转出方客户编号
	PayCusty                string  `xorm:"'pay_custy'"`                 //转出方客户类型
	PayPrductId             string  `xorm:"'pay_prduct_id'"`             //转出方产品号
	PayPrductNm             int     `xorm:"'pay_prduct_nm'"`             //转出方产品顺序号
	PayAgreement            string  `xorm:"'pay_agreement'"`             //转出方合约号
	PayAgreementType        string  `xorm:"'pay_agreement_type'"`        //转出方合约类型
	PayMdsNm                string  `xorm:"'pay_mds_nm'"`                //转出方介质号码
	PayMdsTyp               string  `xorm:"'pay_mds_typ'"`               //转出方介质类型
	PayCurrency             string  `xorm:"'pay_currency'"`              //转出方币种
	PayCashtranFlag         string  `xorm:"'pay_cashtran_flag'"`         //转出方钞汇标识
	PayAccBalance           float64 `xorm:"'pay_acc_balance'"`           //转出方账户余额
	PayAmtAvb               float64 `xorm:"'pay_amt_avb'"`               //转出方可用余额
	PayUscode               string  `xorm:"'pay_uscode'"`                //转出方客户类型
	CltSourceFunds          string  `xorm:"'clt_source_funds'"`          //转入方账号类型
	CltCusid                string  `xorm:"'clt_cusid'"`                 //转入方客户编号
	CltCusty                string  `xorm:"'clt_custy'"`                 //转入方客户类型
	CltPrductId             string  `xorm:"'clt_prduct_id'"`             //转入方产品号
	CltPrductNm             int     `xorm:"'clt_prduct_nm'"`             //转入方产品顺序号
	CltAgreement            string  `xorm:"'clt_agreement'"`             //转入方合约号
	CltAgreementType        string  `xorm:"'clt_agreement_type'"`        //转入方合约类型
	CltMdsNm                string  `xorm:"'clt_mds_nm'"`                //转入方介质号码
	CltMdsTyp               string  `xorm:"'clt_mds_typ'"`               //转入方介质类型
	CltCurrency             string  `xorm:"'clt_currency'"`              //转入方币种
	CltCashtranFlag         string  `xorm:"'clt_cashtran_flag'"`         //转入方钞汇标识
	CltAccBalance           float64 `xorm:"'clt_acc_balance'"`           //转入方账户余额
	CltAmtAvb               float64 `xorm:"'clt_amt_avb'"`               //转入方可用余额
=======
>>>>>>> pre-release_v1.0.0
	TransationAmt           float64 `xorm:"'transation_amt'"`            //交易金额
	PairNm                  string  `xorm:"'pair_nm'"`                   //套号
	BatchNm                 string  `xorm:"'batch_nm'"`                  //批号
	TradeFlag               string  `xorm:"'trade_flag'"`                //反交易标志
	ReverseTradeFlag        string  `xorm:"'reverse_trade_flag'"`        //冲正交易标志
	OriginalTransactionDate string  `xorm:"'original_transaction_date'"` //原交易日期
	OriginalGlobalBizseqNo  string  `xorm:"'original_global_bizseq_no'"` //原全局流水号
	OriginalSrcBizseqNo     string  `xorm:"'original_src_bizseq_no'"`    //原业务流水号
	TranResult              string  `xorm:"'tran_result'"`               //处理状态
	ErrorCode               string  `xorm:"'error_code'"`                //错误码
	ErrorDesc               string  `xorm:"'error_desc'"`                //错误描述
	LastUpDatetime          string  `xorm:"'last_up_datetime'"`          //最后更新日期时间
	LastUpBn                string  `xorm:"'last_up_bn'"`                //最后更新行所
	LastUpEm                string  `xorm:"'last_up_em'"`                //最后更新柜员
}

func (t *T_evsbmxp) TableName() string {
	return "t_evsbmxp"
}

func InsertTevsbmxpOne(tevsbmxp *T_evsbmxp, o *xorm.Engine) error {
	num, err := o.Insert(tevsbmxp)
	if err != nil {
		return err
	}
	if num != 1 {
		return errors.New("Insert tevsbmxp record to database failed", "")
	}
	return nil
}

func DeleteTevsbmxpOne(GlobalBizseqNo string, o *xorm.Engine) error {
	sql := "delete from t_evsbmxp where global_biz_seqno =?"
	_, err := o.Exec(sql, GlobalBizseqNo)
	if err != nil {
		log.Info("delete t_evsbmxp failed")
		return err
	}
	return nil
}

func QueryTevsbmxpById(GlobalBizseqNo string, o *xorm.Engine) (t_evsbmxp *T_evsbmxp, err error) {

	t_evsbmxp = &T_evsbmxp{
		GlobalBizSeqNo: GlobalBizseqNo,
	}
	log.Debug("Query where：", t_evsbmxp)
	exists, err := o.Get(t_evsbmxp)
	if err != nil {
		log.Info("The product information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evsbmxp, nil
}

func UpdateTevsbmxpById(GlobalBizseqNo string, tevsbmxp *T_evsbmxp, o *xorm.Engine) error {
	_, err := o.ID(GlobalBizseqNo).Update(tevsbmxp)
	if err != nil {
		log.Info("Update t_evsbmxp failed")
		return err
	}
	return nil
}

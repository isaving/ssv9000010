//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000010I struct {
<<<<<<< HEAD
	TransationDate          string  `description:"transaction date"`
	TransationTime          string  `description:"transaction hour"`
	GlobalBizseqNo          string  `validate:"required" description:"Global serial number"`
	SrcBizseqNo             string  `description:"Business serial number"`                              //业务流水号
	SrcTimeStamp            string  `description:"Source system working date and time"`                 //源系统工作日期时间
	SrcSysId                string  `description:"Source system encoding"`                              //源系统编码
	SrcDcn                  string  `description:"Source system DCN"`                                   //源系统DCN
	OrgChannelType          string  `description:"Transaction initiation channel"`                      //交易发起渠道
	TxDeviceId              string  `description:"Terminal number"`                                     //终端号
	TxDeptCode              string  `description:"Trading house"`                                       //交易行所
	BnAcc                   string  `description:"Accounting office"`                                   //账务行所
	TransationEm            string  `description:"Transaction teller"`                                  //交易柜员
	TransationType          string  `description:"means of transaction"`                                //交易方式
	PaySourceFunds          string  `description:"Transferring account type"`                           //转出方账号类型
	PayCusid                string  `description:"Transferring client number"`                          //转出方客户编号
	PayCusty                string  `description:"Transferring client type"`                            //转出方客户类型
	PayPrductId             string  `description:"Transferring product number"`                         //转出方产品号
	PayPrductNm             int     `description:"Product serial number of the transferer"`             //转出方产品顺序号
	PayAgreement            string  `description:"Transferring party contract number"`                  //转出方合约号
	PayAgreementType        string  `description:"Transferring contract type"`                          //转出方合约类型
	PayMdsNm                string  `description:"Transferring media number"`                           //转出方介质号码
	PayMdsTyp               string  `description:"Transferring media type"`                             //转出方介质类型
	PayCurrency             string  `description:"Transferring currency"`                               //转出方币种
	PayCashtranFlag         string  `description:"The transferor's currency exchange identification"`   //转出方钞汇标识
	PayAccBalance           float64 `description:"Transferring account balance"`                        //转出方账户余额
	PayAmtAvb               float64 `description:"Transferring account available balance"`              //转出方可用余额
	CltSourceFunds          string  `description:"Transferring account type"`                           //转入方账号类型
	CltCusid                string  `description:"Transfer-in Client Number"`                           //转入方客户编号
	CltCusty                string  `description:"Transfer-in client type"`                             //转入方客户类型
	CltPrductId             string  `description:"Transferring party product number"`                   //转入方产品号
	CltPrductNm             int     `description:"Transferring product serial number"`                  //转入方产品顺序号
	CltAgreement            string  `description:"Transferring party contract number"`                  //转入方合约号
	CltAgreementType        string  `description:"Transferring party contract type"`                    //转入方合约类型
	CltMdsNm                string  `description:"Transfer-in party media number"`                      //转入方介质号码
	CltMdsTyp               string  `description:"Transfer-in media type"`                              //转入方介质类型
	CltCurrency             string  `description:"Transferring currency"`                               //转入方币种
	CltCashtranFlag         string  `description:"Transferring party currency exchange identification"` //转入方钞汇标识
	CltAccBalance           float64 `description:"Transferring party account balance"`                  //转入方账户余额
	CltAmtAvb               float64 `description:"Transferring party account available balance"`        //转入方可用余额
	TransationAmt           float64 `description:"The transaction amount"`                              //交易金额
	PairNm                  string  `description:"Set number"`                                          //套号
	BatchNm                 string  `description:"batch number"`                                        //批号
	TradeFlag               string  `description:"Anti-trading flag"`                                   //反交易标志
	ReverseTradeFlag        string  `description:"Reversal transaction sign"`                           //冲正交易标志
	OriginalTransactionDate string  `description:"Original transaction date"`                           //原交易日期
	OriginalGlobalBizseqNo  string  `description:"Original global serial number"`                       //原全局流水号
	OriginalSrcBizseqNo     string  `description:"Original business serial number"`                     //原业务流水号
	TranResult              string  `description:"Processing status"`                                   //处理状态
	ErrorCode               string  `description:"error code"`                                          //错误码
	LastUpDatetime          string  `description:"Last update date and time"`                           //最后更新日期时间
	LastUpBn                string  `description:"Last update"`                                         //最后更新行所
	LastUpEm                string  `description:"Last update teller"`                                  //最后更新柜员
	PayUscode               string  `description:"Enter use Code"`                                      //转出方用途代码
	CltUscode               string  `description:"Turn out use Code"`                                   //转入方用途代码
=======
	TransationDate          string  `description:"transaction date"`                         //交易日期
	TransationTime          string  `description:"transaction hour"`                         //交易时间
	GlobalBizSeqNo          string  `validate:"required" description:"Global serial number"` //全局流水号
	SrcBizseqNo             string  `description:"Business serial number"`                   //业务流水号
	SrcTimeStamp            string  `description:"Source system working date and time"`      //源系统工作日期时间
	SrcSysId                string  `description:"Source system encoding"`                   //源系统编码
	SrcDcn                  string  `description:"Source system DCN"`                        //源系统DCN
	OrgChannelType          string  `description:"Transaction initiation channel"`           //交易发起渠道
	TxDeviceId              string  `description:"Terminal number"`                          //终端号
	TxDeptCode              string  `description:"Trading house"`                            //交易行所
	BnAcc                   string  `description:"Accounting office"`                        //账务行所
	TransationEm            string  `description:"Transaction teller"`                       //交易柜员
	TransationType          string  `description:"means of transaction"`                     //交易方式
	TransationAmt           float64 `description:"The transaction amount"`                   //交易金额
	PairNm                  string  `description:"Set number"`                               //套号
	BatchNm                 string  `description:"batch number"`                             //批号
	TradeFlag               string  `description:"Anti-trading flag"`                        //反交易标志
	ReverseTradeFlag        string  `description:"Reversal transaction sign"`                //冲正交易标志
	OriginalTransactionDate string  `description:"Original transaction date"`                //原交易日期
	OriginalGlobalBizseqNo  string  `description:"Original global serial number"`            //原全局流水号
	OriginalSrcBizseqNo     string  `description:"Original business serial number"`          //原业务流水号
	TranResult              string  `description:"Processing status"`                        //处理状态
	ErrorCode               string  `description:"error code"`                               //错误码
	ErrorDesc               string  `description:"wrong description"`                        //错误描述
	LastUpDatetime          string  `description:"Last update date and time"`                //最后更新日期时间
	LastUpBn                string  `description:"Last update"`                              //最后更新行所
	LastUpEm                string  `description:"Last update teller"`                       //最后更新柜员
>>>>>>> pre-release_v1.0.0
}

type SSV9000010O struct {
}

// @Desc Build request message
func (o *SSV9000010I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000010I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000010O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000010O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000010I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000010I) GetServiceKey() string {
	return "ssv9000010"
}

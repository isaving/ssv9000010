//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv9000010/constant"
	"git.forms.io/isaving/sv/ssv9000010/dao"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
	"time"
)

var Ssv9000010Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000010",
	ConfirmMethod: "ConfirmSsv9000010",
	CancelMethod:  "CancelSsv9000010",
}

type Ssv9000010 interface {
	TrySsv9000010(*models.SSV9000010I) (*models.SSV9000010O, error)
	ConfirmSsv9000010(*models.SSV9000010I) (*models.SSV9000010O, error)
	CancelSsv9000010(*models.SSV9000010I) (*models.SSV9000010O, error)
}

type Ssv9000010Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv900010O *models.SSV9000010O
	Ssv900010I *models.SSV9000010I
	O          *xorm.Engine
}

// @Desc Ssv9000010 process
// @Author
// @Date 2020-12-12
func (impl *Ssv9000010Impl) TrySsv9000010(ssv9000010I *models.SSV9000010I) (ssv9000010O *models.SSV9000010O, err error) {
	//TODO Service Business Process,No Need Validate Paramer,Paramer Has Already Validate in Controller
	impl.Ssv900010I = ssv9000010I
	impl.O = dao.EngineCache
	QueryTevsbmxpRecord, err := dao.QueryTevsbmxpById(impl.Ssv900010I.GlobalBizSeqNo, impl.O)
	if err != nil {
		log.Info("QueryTevsbmxpById failed")
		return nil, err
	}
	if QueryTevsbmxpRecord == nil {
		return nil, errors.New("Query T_evsbmxp no records！", constants.ERRCODE3)
	}

	Tevsbmxp := &dao.T_evsbmxp{
		TransationDate:          impl.Ssv900010I.TransationDate,
		TransationTime:          impl.Ssv900010I.TransationTime,
		SrcBizseqNo:             impl.Ssv900010I.SrcBizseqNo,
		SrcTimeStamp:            impl.Ssv900010I.SrcTimeStamp,
		SrcSysId:                impl.Ssv900010I.SrcSysId,
		SrcDcn:                  impl.Ssv900010I.SrcDcn,
		OrgChannelType:          impl.Ssv900010I.OrgChannelType,
		TxDeviceId:              impl.Ssv900010I.TxDeviceId,
		TxDeptCode:              impl.Ssv900010I.TxDeptCode,
		BnAcc:                   impl.Ssv900010I.BnAcc,
		TransationEm:            impl.Ssv900010I.TransationEm,
		TransationType:          impl.Ssv900010I.TransationType,
<<<<<<< HEAD
		PaySourceFunds:          impl.Ssv900010I.PaySourceFunds,
		PayCusid:                impl.Ssv900010I.PayCusid,
		PayCusty:                impl.Ssv900010I.PayCusty,
		PayPrductId:             impl.Ssv900010I.PayPrductId,
		PayPrductNm:             impl.Ssv900010I.PayPrductNm,
		PayAgreement:            impl.Ssv900010I.PayAgreement,
		PayAgreementType:        impl.Ssv900010I.PayAgreementType,
		PayMdsNm:                impl.Ssv900010I.PayMdsNm,
		PayMdsTyp:               impl.Ssv900010I.PayMdsTyp,
		PayCurrency:             impl.Ssv900010I.PayCurrency,
		PayCashtranFlag:         impl.Ssv900010I.PayCashtranFlag,
		PayAccBalance:           impl.Ssv900010I.PayAccBalance,
		PayAmtAvb:               impl.Ssv900010I.PayAmtAvb,
		PayUscode:               impl.Ssv900010I.PayUscode,
		CltSourceFunds:          impl.Ssv900010I.CltSourceFunds,
		CltCusid:                impl.Ssv900010I.CltCusid,
		CltCusty:                impl.Ssv900010I.CltCusty,
		CltPrductId:             impl.Ssv900010I.CltPrductId,
		CltPrductNm:             impl.Ssv900010I.CltPrductNm,
		CltAgreement:            impl.Ssv900010I.CltAgreement,
		CltAgreementType:        impl.Ssv900010I.CltAgreementType,
		CltMdsNm:                impl.Ssv900010I.CltMdsNm,
		CltMdsTyp:               impl.Ssv900010I.CltMdsTyp,
		CltCurrency:             impl.Ssv900010I.CltCurrency,
		CltCashtranFlag:         impl.Ssv900010I.CltCashtranFlag,
		CltAccBalance:           impl.Ssv900010I.CltAccBalance,
		CltAmtAvb:               impl.Ssv900010I.CltAmtAvb,
=======
>>>>>>> pre-release_v1.0.0
		TransationAmt:           impl.Ssv900010I.TransationAmt,
		CltUscode:               impl.Ssv900010I.CltUscode,
		PairNm:                  impl.Ssv900010I.PairNm,
		BatchNm:                 impl.Ssv900010I.BatchNm,
		TradeFlag:               impl.Ssv900010I.TradeFlag,
		ReverseTradeFlag:        impl.Ssv900010I.ReverseTradeFlag,
		OriginalTransactionDate: impl.Ssv900010I.OriginalTransactionDate,
		OriginalGlobalBizseqNo:  impl.Ssv900010I.OriginalGlobalBizseqNo,
		OriginalSrcBizseqNo:     impl.Ssv900010I.OriginalSrcBizseqNo,
		TranResult:              impl.Ssv900010I.TranResult,
		ErrorCode:               impl.Ssv900010I.ErrorCode,
		LastUpDatetime:          time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:                impl.SrcAppProps[constant.TXDEPTCODE],
		LastUpEm:                impl.SrcAppProps[constant.TXUSERID],
	}
	err = dao.UpdateTevsbmxpById(impl.Ssv900010I.GlobalBizSeqNo, Tevsbmxp, impl.O)
	if err != nil {
		log.Info("UpdateTevsbmxpById failed")
		return nil, err
	}
	ssv9000010O = &models.SSV9000010O{
		//TODO Assign  value to the OUTPUT Struct field
	}

	return ssv9000010O, nil
}

func (impl *Ssv9000010Impl) ConfirmSsv9000010(ssv9000010I *models.SSV9000010I) (ssv9000010O *models.SSV9000010O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv9000010")

	return nil, nil
}

func (impl *Ssv9000010Impl) CancelSsv9000010(ssv9000010I *models.SSV9000010I) (ssv9000010O *models.SSV9000010O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv9000010")
	return nil, nil
}

//Version: 1.0.0
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000010/dao"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}

func TestSsv9000009Impl_Ssv9000009_Success(t *testing.T) {

	input := &models.SSV9000010I{
		TranResult: "1",
		ErrorCode:  "SVCM000001",
	}

	impl := Ssv9000010Impl{
		Ssv900010I: input,
		O:          dao.EngineCache,
	}
	maps := make(map[string]string)
	maps["GlobalBizSeqNo"] = "10013"
	impl.SrcAppProps = maps
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_evsbmxp").
				WillReturnRows(sqlmock.NewRows([]string{"global_biz_seqno"}).AddRow("1000001"))
			//mock.ExpectExec("t_evsbmxp").WithArgs(input).WillReturnResult(sqlmock.NewResult(0, 1))
			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000010(input)
				//_, _ = impl.ConfirmSsv9000010(input)
				_, _ = impl.CancelSsv9000010(input)
			})
		})

	})
}

func TestSsv9000009Impl_Ssv9000009_Success1(t *testing.T) {

	input := &models.SSV9000010I{
		TranResult: "1",
		ErrorCode:  "SVCM000001",
	}

	impl := Ssv9000010Impl{
		Ssv900010I: input,
		O:          dao.EngineCache,
	}
	maps := make(map[string]string)
	maps["GlobalBizSeqNo"] = "10013"
	impl.SrcAppProps = maps
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectExec("t_evsbmxp").WithArgs(input).WillReturnResult(sqlmock.NewResult(0, 1))
			Convey("Validate session", func() {
				//_, _ = impl.TrySsv9000010(input)
				_, _ = impl.ConfirmSsv9000010(input)
				//_, _ = impl.CancelSsv9000010(input)
			})
		})

	})
}

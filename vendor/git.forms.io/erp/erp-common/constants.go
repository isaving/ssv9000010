package erp_common

const (
	APPPROPS_ACTIVITYID = "activityId"
	APPPROPS_SESSIONID  = "sessionId"
	APPPROPS_CHANNEL    = "channel"
)

const (
	DLS_TYPE_CMM  = "CMM"
	DLS_ID_COMMON = "common"
	DLS_TYPE_SC   = "SC"
	DLS_TYPE_CID  = "CID"
	DLS_TYPE_PID  = "PID"
)

const (
	DATE_FORMAT     = "2006-01-02"
	TIME_FORMAT     = "15:04:05"
	DATETIME_FORMAT = "2006-01-02 15:04:05"
)

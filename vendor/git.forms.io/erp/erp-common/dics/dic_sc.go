package dics

const (
	STATUS_SHOPPINGCART_OPEN   = "0"
	STATUS_SHOPPINGCART_CANCEL = "1"
	STATUS_SHOPPINGCART_CLOSE  = "2"
)

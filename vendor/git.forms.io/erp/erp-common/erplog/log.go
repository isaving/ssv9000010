package erplog

import (
	"git.forms.io/universe/solapp-sdk/log"
)

type ErpLog struct {
	ActivityId string
}

func (el ErpLog) format(format string) string {
	return el.ActivityId + " - " + format
}

func (el ErpLog) Debugf(format string, args ...interface{}) {
	log.Infof(el.format(format), args...)
}

func (el ErpLog) Debug(args ...interface{}) {
	log.Debug(el.ActivityId, args)
}

func (el ErpLog) Infof(format string, args ...interface{}) {
	log.Infof(el.format(format), args...)
}

func (el ErpLog) Info(args ...interface{}) {
	log.Info(el.ActivityId, args)
}

func (el ErpLog) Errorf(format string, args ...interface{}) {
	log.Errorf(el.format(format), args...)
}

func (el ErpLog) Error(args ...interface{}) {
	log.Error(el.ActivityId, args)
}

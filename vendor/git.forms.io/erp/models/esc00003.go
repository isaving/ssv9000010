//Version: 1.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type ESC00003I struct {
	CartId         string  `json:"cartId"`
	Barcode        string  `json:"barcode"`
	ProductStoreId string  `json:"productStoreId" validate:"required"`
	ProductId      string  `json:"productId"`
	Quantity       float64 `json:"quantity"`
}

type ESC00003O struct {
	CartId string `json:"cartId"`
	ItemId string `json:"itemId"`
}

// @Desc Build request message
func (o *ESC00003I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *ESC00003I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *ESC00003O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ESC00003O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *ESC00003I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*ESC00003I) GetServiceKey() string {
	return "esc00003"
}

//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000001I struct {
	AgreementId     string  `validate:"required",json:"AgreementId"`
	PrductId        string  `validate:"required",json:"PrductId"`
	PrductNm        int     `validate:"required",json:"PrductNm"`
	AgreementType   string  `validate:"required",json:"AgreementType"`
	Currency        string  `validate:"required",json:"Currency"`
	PeriodFlag      string  `validate:"required",json:"PeriodFlag"`
	CashTranFlag    string  `validate:"required",json:"CashTranFlag"`
	CstmrId         string  `validate:"required",json:"CstmrId"`
	CstmrTyp        string  `validate:"required",json:"CstmrTyp"`
	AccuntNme       string  `validate:"required",json:"AccuntNme"`
	UsgCod          string  `validate:"required",json:"UsgCod"`
	WdrwlMthd       string  `validate:"required",json:"WdrwlMthd"`
	AccPsw          string  `validate:"required",json:"AccPsw"`
	TrnWdrwMthd     string  `validate:"required",json:"TrnWdrwMthd"`
	DepCreFlag      string  `validate:"required",json:"DepCreFlag"`
	AccFlag         string  `validate:"required",json:"AccFlag"`
	Account         string  `validate:"required",json:"Account"`
	TermFlag        string  `validate:"required",json:""`
	BegnTxDt        string  `validate:"required",json:"BegnTxDt"`
	AppntDtFlg      string  `validate:"required",json:"AppntDtFlg"`
	AppntDt         string  `validate:"required",json:"AppntDt"`
	OpnAmt          float64 `validate:"required",json:"OpnAmt"`
	AccCnclFlg      string  `validate:"required",json:"AccCnclFlg"`
	AutCnl          string  `validate:"required",json:"AutCnl"`
	DytoCncl        int     `validate:"required",json:"DytoCncl"`
	SttmntFlg       string  `validate:"required",json:"SttmntFlg"`
	WthdrwlMthd     string  `validate:"required",json:"WthdrwlMthd"`
	OvrDrwFlg       string  `validate:"required",json:"OvrDrwFlg"`
	DpTerm          int     `validate:"required",json:"DpTerm"`
	DpTermUn        string  `validate:"required",json:"DpTermUn"`
	HolidayFlag     string  `validate:"required",json:"HolidayFlag"`
	AdvnWithdTm     int     `validate:"required",json:"AdvnWithdTm"`
	MatryWithdTm    int     `validate:"required",json:"MatryWithdTm"`
	VerDueWithdTm   int     `validate:"required",json:"VerDueWithdTm"`
	LwWithAmt       float64 `validate:"required",json:"LwWithAmt"`
	LwKepAmt        float64 `validate:"required",json:"LwKepAmt"`
	AprvWithDt      string  `validate:"required",json:"AprvWithDt"`
	GrcWithDy       int     `validate:"required",json:"GrcWithDy"`
	AllwDpsit       int     `validate:"required",json:"AllwDpsit"`
	DuePrcesFlag    string  `validate:"required",json:"DuePrcesFlag"`
	PrncplPrcesFlag string  `validate:"required",json:"PrncplPrcesFlag"`
	MinRenAmt       float64 `validate:"required",json:"MinRenAmt"`
	RenNm           string  `validate:"required",json:"RenNm"`
	PrncplRenFlag   string  `validate:"required",json:"PrncplRenFlag"`
	TexRenFlag      string  `validate:"required",json:"TexRenFlag"`
	CstmrCntctAdd   string  `validate:"required",json:"CstmrCntctAdd"`
	CstmrCntctPh    string  `validate:"required",json:"CstmrCntctPh"`
	CstmrCntctEm    string  `validate:"required",json:"CstmrCntctPh"`
	BnCrtAcc        string  `validate:"required",json:"BnCrtAcc"`
	BnAcc           string  `validate:"required",json:"BnACC"`
	AccOpnDt        string  `validate:"required",json:"AccOpnDt"`
	AccOpnEm        string  `validate:"required",json:"AccOpnEm"`
	RequrstId       string  `validate:"required",json:"RequrstID"`
}

type SSV9000001O struct {
	AgreementID string
}

// @Desc Build request message
func (o *SSV9000001I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000001I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000001O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000001O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000001I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000001I) GetServiceKey() string {
	return "ssv9000001"
}

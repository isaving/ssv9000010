package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060010I struct {
	AcctgAcctNo		string `validate:"required,max=20"`
	IntRateNo     string  `valid:"Required;MaxSize(10)"` //利率编号
	BankNo        string  `valid:"Required;MaxSize(30)"` //机构号
	Currency      string  `valid:"Required;MaxSize(3)"`  //币种代码
	IntRate       float64 `valid:"Required"`             //利率
	EffectDate    string  `valid:"Required;MaxSize(10)"` //生效日期
	ExpFlag       string  `valid:"Required;MaxSize(1)"`  //失效标志
	DayModfyFlag  string  `valid:"Required;MaxSize(1)"`  //当日修改标志
	IntRateName   string  `valid:"Required;MaxSize(60)"` //利率名称
	IntRateMarket string  `valid:"Required;MaxSize(2)"`  //利率市场说明
	IntRateUnit   string  `valid:"Required;MaxSize(2)"`  //利率类型代码
	LastMaintBrno string  `valid:"Required;MaxSize(30)"` //最后修改机构号
	LastMaintTell string  `valid:"Required;MaxSize(30)"` //最后修改柜员号
}

type AC060010O struct {
	Status	string
}

type AC060010IDataForm struct {
	FormHead CommonFormHead
	FormData AC060010I
}

type AC060010ODataForm struct {
	FormHead CommonFormHead
	FormData AC060010O
}

type AC060010RequestForm struct {
	Form []AC060010IDataForm
}

type AC060010ResponseForm struct {
	Form []AC060010ODataForm
}

// @Desc Build request message
func (o *AC060010RequestForm) PackRequest(AC060010I AC060010I) (responseBody []byte, err error) {

	requestForm := AC060010RequestForm{
		Form: []AC060010IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060010I",
				},
				FormData: AC060010I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060010RequestForm) UnPackRequest(request []byte) (AC060010I, error) {
	AC060010I := AC060010I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060010I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060010I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060010ResponseForm) PackResponse(AC060010O AC060010O) (responseBody []byte, err error) {
	responseForm := AC060010ResponseForm{
		Form: []AC060010ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060010O",
				},
				FormData: AC060010O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060010ResponseForm) UnPackResponse(request []byte) (AC060010O, error) {

	AC060010O := AC060010O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060010O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060010O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060010I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

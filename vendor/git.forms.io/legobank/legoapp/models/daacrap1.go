package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRAP1I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
	PeriodNum		int64
	PeriodNum1		int64
}

type DAACRAP1O struct {
	LogTotCount string    `json:"LogTotCount"`
	Records     []DAACRAP1ORecords `json:"Records"`
}

type DAACRAP1ORecords struct {
	AcctgAcctNo              string `json:"AcctgAcctNo"`
	AcctgDate                string `json:"AcctgDate"`
	ActualRepayBal           int    `json:"ActualRepayBal"`
	ActualRepayDate          string `json:"ActualRepayDate"`
	BurningSum               int    `json:"BurningSum"`
	CurrPeriodIntacrBgnDt    string `json:"CurrPeriodIntacrBgnDt"`
	CurrPeriodIntacrEndDt    string `json:"CurrPeriodIntacrEndDt"`
	CurrPeriodRepayDt        string `json:"CurrPeriodRepayDt"`
	CurrentPeriodUnStillPrin int    `json:"CurrentPeriodUnStillPrin"`
	CurrentStatus            string `json:"CurrentStatus"`
	LastMaintBrno            string `json:"LastMaintBrno"`
	LastMaintDate            string `json:"LastMaintDate"`
	LastMaintTell            string `json:"LastMaintTell"`
	LastMaintTime            string `json:"LastMaintTime"`
	PeriodNum                int    `json:"PeriodNum"`
	PlanRepayBal             int    `json:"PlanRepayBal"`
	RepayStatus              string `json:"RepayStatus"`
	TccState                 int    `json:"TccState"`
	ValueDate                string `json:"ValueDate"`
}

type DAACRAP1IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRAP1I
}

type DAACRAP1ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRAP1O
}

type DAACRAP1RequestForm struct {
	Form []DAACRAP1IDataForm
}

type DAACRAP1ResponseForm struct {
	Form []DAACRAP1ODataForm
}

// @Desc Build request message
func (o *DAACRAP1RequestForm) PackRequest(DAACRAP1I DAACRAP1I) (responseBody []byte, err error) {

	requestForm := DAACRAP1RequestForm{
		Form: []DAACRAP1IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAP1I",
				},
				FormData: DAACRAP1I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRAP1RequestForm) UnPackRequest(request []byte) (DAACRAP1I, error) {
	DAACRAP1I := DAACRAP1I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAP1I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAP1I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRAP1ResponseForm) PackResponse(DAACRAP1O DAACRAP1O) (responseBody []byte, err error) {
	responseForm := DAACRAP1ResponseForm{
		Form: []DAACRAP1ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRAP1O",
				},
				FormData: DAACRAP1O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRAP1ResponseForm) UnPackResponse(request []byte) (DAACRAP1O, error) {

	DAACRAP1O := DAACRAP1O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRAP1O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRAP1O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRAP1I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

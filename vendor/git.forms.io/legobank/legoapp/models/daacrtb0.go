package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTB0I struct {
	AcctgAcctNo   string ` validate:"required,max=20"`
	PrinStatus    string
	ValidFlag     string
}

type DAACRTB0O struct {
	AcctgAcctNo   string
	PrinStatus    string
	IntPlanNo     string
	OrgCreate     string
	ValidFlag     string
	Bk1           string
	Bk2           string
	Bk3           string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string

}

type DAACRTB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTB0I
}

type DAACRTB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTB0O
}

type DAACRTB0RequestForm struct {
	Form []DAACRTB0IDataForm
}

type DAACRTB0ResponseForm struct {
	Form []DAACRTB0ODataForm
}

// @Desc Build request message
func (o *DAACRTB0RequestForm) PackRequest(DAACRTB0I DAACRTB0I) (responseBody []byte, err error) {

	requestForm := DAACRTB0RequestForm{
		Form: []DAACRTB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTB0I",
				},
				FormData: DAACRTB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTB0RequestForm) UnPackRequest(request []byte) (DAACRTB0I, error) {
	DAACRTB0I := DAACRTB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTB0ResponseForm) PackResponse(DAACRTB0O DAACRTB0O) (responseBody []byte, err error) {
	responseForm := DAACRTB0ResponseForm{
		Form: []DAACRTB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTB0O",
				},
				FormData: DAACRTB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTB0ResponseForm) UnPackResponse(request []byte) (DAACRTB0O, error) {

	DAACRTB0O := DAACRTB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

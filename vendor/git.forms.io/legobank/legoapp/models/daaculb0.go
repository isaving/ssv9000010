package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULB0I struct {
	//输入是个map
}

type DAACULB0O struct {

}

type DAACULB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULB0I
}

type DAACULB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULB0O
}

type DAACULB0RequestForm struct {
	Form []DAACULB0IDataForm
}

type DAACULB0ResponseForm struct {
	Form []DAACULB0ODataForm
}

// @Desc Build request message
func (o *DAACULB0RequestForm) PackRequest(DAACULB0I DAACULB0I) (responseBody []byte, err error) {

	requestForm := DAACULB0RequestForm{
		Form: []DAACULB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULB0I",
				},
				FormData: DAACULB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULB0RequestForm) UnPackRequest(request []byte) (DAACULB0I, error) {
	DAACULB0I := DAACULB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULB0ResponseForm) PackResponse(DAACULB0O DAACULB0O) (responseBody []byte, err error) {
	responseForm := DAACULB0ResponseForm{
		Form: []DAACULB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULB0O",
				},
				FormData: DAACULB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULB0ResponseForm) UnPackResponse(request []byte) (DAACULB0O, error) {

	DAACULB0O := DAACULB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

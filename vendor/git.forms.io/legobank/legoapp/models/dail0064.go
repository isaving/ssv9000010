package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAIL0064I []struct {
	AcctiAcctNo		string
	Pridnum			string
}

type DAIL0064O []struct {
	AccmIntSetlAmt int `json:"AccmIntSetlAmt"`
	AcruUnstlIntr  int `json:"AcruUnstlIntr"`
	PlanRepayBal   int `json:"PlanRepayBal"`
}

type DAIL0064IDataForm struct {
	FormHead CommonFormHead
	FormData DAIL0064I
}

type DAIL0064ODataForm struct {
	FormHead CommonFormHead
	FormData DAIL0064O
}

type DAIL0064RequestForm struct {
	Form []DAIL0064IDataForm
}

type DAIL0064ResponseForm struct {
	Form []DAIL0064ODataForm
}

// @Desc Build request message
func (o *DAIL0064RequestForm) PackRequest(DAIL0064I DAIL0064I) (responseBody []byte, err error) {

	requestForm := DAIL0064RequestForm{
		Form: []DAIL0064IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL0064I",
				},
				FormData: DAIL0064I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAIL0064RequestForm) UnPackRequest(request []byte) (DAIL0064I, error) {
	DAIL0064I := DAIL0064I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAIL0064I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL0064I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAIL0064ResponseForm) PackResponse(DAIL0064O DAIL0064O) (responseBody []byte, err error) {
	responseForm := DAIL0064ResponseForm{
		Form: []DAIL0064ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAIL0064O",
				},
				FormData: DAIL0064O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAIL0064ResponseForm) UnPackResponse(request []byte) (DAIL0064O, error) {

	DAIL0064O := DAIL0064O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAIL0064O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAIL0064O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAIL0064I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

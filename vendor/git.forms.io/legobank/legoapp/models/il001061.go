package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL001061I struct {
	LoginOrgNo	string	`Required`
	Empnbr		string	`Required`
	PwdBlock	string	`Required`
}

type IL001061O struct {
	Empnbr		string
	RoleId		string
	OrgNbr		string
	EmplyName		string
	RecntMtguarDate		string
	RecntMtguarTime		string
	MtguarStatus		string
	Status		string
	OrgNm		string
	RoleNm		string
	FuncUrl		string
}

type IL001061IDataForm struct {
	FormHead CommonFormHead
	FormData IL001061I
}

type IL001061ODataForm struct {
	FormHead CommonFormHead
	FormData IL001061O
}

type IL001061RequestForm struct {
	Form []IL001061IDataForm
}

type IL001061ResponseForm struct {
	Form []IL001061ODataForm
}

// @Desc Build request message
func (o *IL001061RequestForm) PackRequest(IL001061I IL001061I) (responseBody []byte, err error) {

	requestForm := IL001061RequestForm{
		Form: []IL001061IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL001061I",
				},
				FormData: IL001061I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL001061RequestForm) UnPackRequest(request []byte) (IL001061I, error) {
	IL001061I := IL001061I{}
	if err := json.Unmarshal(request, o); nil != err {
		return IL001061I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL001061I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL001061ResponseForm) PackResponse(IL001061O IL001061O) (responseBody []byte, err error) {
	responseForm := IL001061ResponseForm{
		Form: []IL001061ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL001061O",
				},
				FormData: IL001061O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL001061ResponseForm) UnPackResponse(request []byte) (IL001061O, error) {

	IL001061O := IL001061O{}

	if err := json.Unmarshal(request, o); nil != err {
		return IL001061O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return IL001061O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL001061I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

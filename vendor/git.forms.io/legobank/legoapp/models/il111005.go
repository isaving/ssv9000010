package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL111005IDataForm struct {
	FormHead CommonFormHead
	FormData IL111005I
}

type IL111005ODataForm struct {
	FormHead CommonFormHead
	FormData IL111005O
}

type IL111005RequestForm struct {
	Form []IL111005IDataForm
}

type IL111005ResponseForm struct {
	Form []IL111005ODataForm
}

type IL111005I struct {
	LoanDubilNo    string `json:"LoanDubilNo"`                       // 贷款借据号
	MakelnAplySn   string `validate:"required",json:"MakelnAplySn"`  // 放款申请流水号
	CustNo         string `validate:"required",json:"CustNo"`        // 客户编号
	LoanStatus     string `validate:"required",json:"LoanStatus"`    // 贷款状态
	ApproveUserId  string `validate:"required",json:"ApproveUserId"` // 审批人员工号
	ApproveView    string `validate:"required",json:"ApproveView"`   // 审批意见
	ApproveComment string `json:"ApproveComment"`                    // 审批意见说明
}

type IL111005O struct {
	Status      string `json:"Status"`      // 返回信息
	LoanDubilNo string `json:"LoanDubilNo"` // 贷款借据号
}

// @Desc Build request message
func (o *IL111005RequestForm) PackRequest(il111005I IL111005I) (responseBody []byte, err error) {

	requestForm := IL111005RequestForm{
		Form: []IL111005IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL111005I",
				},
				FormData: il111005I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL111005RequestForm) UnPackRequest(request []byte) (IL111005I, error) {
	il111005I := IL111005I{}
	if err := json.Unmarshal(request, o); nil != err {
		return il111005I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il111005I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL111005ResponseForm) PackResponse(il111005O IL111005O) (responseBody []byte, err error) {
	responseForm := IL111005ResponseForm{
		Form: []IL111005ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL111005O",
				},
				FormData: il111005O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL111005ResponseForm) UnPackResponse(request []byte) (IL111005O, error) {

	il111005O := IL111005O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il111005O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il111005O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL111005I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

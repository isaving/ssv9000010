package util

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/models"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/beego/i18n"
	"github.com/pkg/errors"
)

//@Desc Build normal response
func BuildResponse(request *client.UserMessage, requestHeader map[string]string, rspFormId string, data map[string]interface{}) (*client.UserMessage, error) {

	log.Debug("start Build Response")

	rspBodyMsg := models.Body{
		Form: []models.Form{
			{
				FormHead: models.FormHead{
					FormId: rspFormId,
				},
				FormData: data,
			},
		},
	}
	rspBody, err := json.Marshal(rspBodyMsg)
	if err != nil {
		return request, errors.Errorf("response models body build failed. %v", err)
	}
	setHeader(request, requestHeader)
	if _, ok := request.AppProps[constant.TRGBIZSEQNO]; !ok {
		request.AppProps[constant.TRGBIZSEQNO] = getBusinessSerialNo()
	}
	rspMsg := &client.UserMessage{
		Id:             request.Id,
		TopicAttribute: request.TopicAttribute,
		NeedReply:      request.NeedReply,
		NeedAck:        request.NeedAck,
		AppProps:       request.AppProps,
		Body:           rspBody,
	}
	if rspMsg.AppProps != nil {
		rspMsg.AppProps[constant.RETSTATUS] = "N"
	}
	return rspMsg, nil
}

//@Desc Build error response
func BuildErrorResponse(request *client.UserMessage, requestHeader map[string]string, errCode string, msg ...interface{}) (*client.UserMessage, error) {

	log.Debug("start Build Error Response")

	lang, ok := request.AppProps[constant.USERLANG]
	if !ok {
		lang = constant.ENUS
	}
	errMsg := i18n.Tr(lang, "errors."+errCode, msg)
	rspBodyMsg := models.Body{
		Form: []models.Form{
			{
				FormHead: models.FormHead{
					FormId: constant.ERRORFORMID,
				},
				FormData: map[string]interface{}{
					constant.RETMSGCODE: errCode,
					constant.RETMESSAGE: errMsg,
				},
			},
		},
	}
	rspBody, err := json.Marshal(rspBodyMsg)
	if err != nil {
		return request, errors.Errorf("response models body build failed. %v", err)
	}
	setHeader(request, requestHeader)
	if _, ok := request.AppProps[constant.TRGBIZSEQNO]; !ok {
		request.AppProps[constant.TRGBIZSEQNO] = getBusinessSerialNo()
	}
	request.AppProps[constant.RETMSGCODE] = errCode
	request.AppProps[constant.RETMESSAGE] = errMsg
	response := &client.UserMessage{
		Id:             request.Id,
		TopicAttribute: request.TopicAttribute,
		NeedReply:      request.NeedReply,
		NeedAck:        request.NeedAck,
		AppProps:       request.AppProps,
		Body:           rspBody,
	}
	if response.AppProps != nil {
		response.AppProps[constant.RETSTATUS] = "F"
	}
	return response, errors.Errorf("response failed code %v message %v", errCode, errMsg)
}

func BuildErrorResponseMulti(request *client.UserMessage, requestHeader map[string]string, rspFormId string, data map[string]interface{}, errCode string, msg ...interface{}) (*client.UserMessage, error) {

	log.Debug("start Build Error Response")

	lang, ok := request.AppProps[constant.USERLANG]
	if !ok {
		lang = constant.ENUS
	}
	errMsg := i18n.Tr(lang, "errors."+errCode, msg)
	rspBodyMsg := models.Body{
		Form: []models.Form{
			{
				FormHead: models.FormHead{
					FormId: constant.ERRORFORMID,
				},
				FormData: map[string]interface{}{
					constant.RETMSGCODE: errCode,
					constant.RETMESSAGE: errMsg,
				},
			},
			{
				FormHead: models.FormHead{
					FormId: rspFormId,
				},
				FormData: data,
			},
		},
	}
	rspBody, err := json.Marshal(rspBodyMsg)
	if err != nil {
		return request, errors.Errorf("response models body build failed. %v", err)
	}
	setHeader(request, requestHeader)
	if _, ok := request.AppProps[constant.TRGBIZSEQNO]; !ok {
		request.AppProps[constant.TRGBIZSEQNO] = getBusinessSerialNo()
	}
	request.AppProps[constant.RETMSGCODE] = errCode
	request.AppProps[constant.RETMESSAGE] = errMsg
	response := &client.UserMessage{
		Id:             request.Id,
		TopicAttribute: request.TopicAttribute,
		NeedReply:      request.NeedReply,
		NeedAck:        request.NeedAck,
		AppProps:       request.AppProps,
		Body:           rspBody,
	}
	if response.AppProps != nil {
		response.AppProps[constant.RETSTATUS] = "F"
	}
	return response, errors.Errorf("response failed code %v message %v", errCode, errMsg)
}

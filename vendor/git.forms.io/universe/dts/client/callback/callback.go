//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package callback

import (
	"git.forms.io/universe/dts/client/repository"
	constant "git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/dts/common/log"
	"github.com/go-errors/errors"
	"reflect"
)

// @Desc new a TxnCallback which contains transaction's repository
// @Return TxnCallback
func NewTxnCallback() TxnCallback {
	return &DefaultLocalTxnCallback{
		CmpTxnRep: repository.CmpTxnRep,
	}
}

// @Desc  the interface of TxnCallback, contains two call-back methods
type TxnCallback interface {
	Confirm(rootXid string, branchXid string) (int, error)
	Cancel(rootXid string, branchXid string) (int, error)
}

// @Desc  a struct which has interface of repository.CmpTxnRep
type DefaultLocalTxnCallback struct {
	CmpTxnRep repository.CompensableTxnRepository
}

// @Desc call back service's confirm method
// @Param rootXid the id of root transaction
// @Param branchXid the id of branch transaction
// @Return errorCode the result code of confirm method
// @Return err error
func (d *DefaultLocalTxnCallback) Confirm(rootXid string, branchXid string) (errorCode int, err error) {
	log.Debugf("start confirm local transaction root id[%s] | branch id[%s]", rootXid, branchXid)
	txn := d.CmpTxnRep.FindTxnByXid(rootXid, branchXid)
	if txn == nil {
		errorCode = constant.TryResultReportFailedBranchConfirmFailed
		err = errors.Errorf("Cannot found the transaction with rootXid:[%s] | branchXid:[%s]", rootXid, branchXid)
		log.Errorf(err.Error())
		return
	}

	if txn.IsState(constant.CONFIRMING) {
		errorCode = constant.TryResultReportFailedBranchConfirmFailed
		err = errors.Errorf("Transaction is confirming rootXid:[%s] | branchXid:[%s]", rootXid, branchXid)
		log.Errorf(err.Error())
		return
	} else if txn.IsState(constant.CONFIRMED_OK) {
		errorCode = constant.CANNOT_FOUND_ROOT_XID_ERROR_CODE
		err = errors.Errorf("Transaction was confirmed with rootXid:[%s] | branchXid:[%s]", rootXid, branchXid)
		log.Errorf(err.Error())
		return
	}

	txn.SetState(constant.CONFIRMING)
	txnInstance := reflect.ValueOf(txn.TxnInstance)
	confirmMethod := txnInstance.MethodByName(txn.CompensableTxn.ConfirmMethod)
	targetMethodResult := confirmMethod.Call(txn.StoreParams)
	lastResultObj := targetMethodResult[len(targetMethodResult)-1].Interface()
	if nil != lastResultObj {
		err = lastResultObj.(error)
		txn.SetState(constant.CONFIRMED_FAILED)
		log.Errorf("confirm local transaction root id[%s] | branch id[%s] failed, target method execute error message:[%s]", rootXid, branchXid, err.Error())
	} else {
		txn.SetState(constant.CONFIRMED_OK)
		// Confirm执行成功，移除对应的本地事务存储
		d.CmpTxnRep.DeleteTxnByXid(rootXid, branchXid)
		log.Debugf("confirm local transaction root id[%s] | branch id[%s] successfully!", rootXid, branchXid)
	}
	return
}

// @Desc call back service's cancel method
// @Param rootXid the id of root transaction
// @Param branchXid the id of branch transaction
// @Return errorCode the result code of cancel method
// @Return err error
func (d *DefaultLocalTxnCallback) Cancel(rootXid string, branchXid string) (errorCode int, err error) {
	log.Debugf("start cancel local transaction root id[%s] | branch id[%s]", rootXid, branchXid)
	txn := d.CmpTxnRep.FindTxnByXid(rootXid, branchXid)

	defer func() {
		if r := recover(); r != nil {
			txn.SetState(constant.CANCELLED_FAILED)
			log.Errorf("Cancel catch panic, error=%++v", r)
			err = errors.Errorf("Cancel catch panic, error=%v", r)
			return
		}
	}()

	if txn == nil {
		errorCode = constant.CANNOT_FOUND_ROOT_XID_ERROR_CODE
		err = errors.Errorf("Cannot found the transaction with rootXid:[%s] | branchXid:[%s]", rootXid, branchXid)
		log.Errorf(err.Error())
		return
	}

	if txn.IsState(constant.TRYING) {
		// cancel request before trying done, mark transaction need to cancel when try done
		log.Errorf("transaction root id[%s] | branch id[%s] cancel suspend!", rootXid, branchXid)
		txn.SetCancelSuspend()
		return
	} else if !txn.IsState(constant.TRIED_OK) && !txn.IsState(constant.CANCELLED_FAILED) {
		log.Errorf("transaction root id[%s] | branch id[%s] try execute failed, skip cancel!", rootXid, branchXid)
		return
	}

	txn.SetState(constant.CANCELLING)
	txnInstance := reflect.ValueOf(txn.TxnInstance)
	cancelMethod := txnInstance.MethodByName(txn.CompensableTxn.CancelMethod)
	targetMethodResult := cancelMethod.Call(txn.StoreParams)
	lastResultObj := targetMethodResult[len(targetMethodResult)-1].Interface()
	if nil != lastResultObj {
		err = lastResultObj.(error)
		txn.SetState(constant.CANCELLED_FAILED)
		log.Errorf("cancel local transaction root id[%s] | branch id[%s] failed, target method execute error message:[%s]", rootXid, branchXid, err.Error())
	} else {
		txn.SetState(constant.CANCELLED_OK)
		// Cancel执行成功，移除对应的本地事务存储
		d.CmpTxnRep.DeleteTxnByXid(rootXid, branchXid)
		log.Debugf("cancel local transaction root id[%s] | branch id[%s] successfully!", rootXid, branchXid)
	}
	return
}
